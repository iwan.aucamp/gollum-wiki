require 'rubygems'
#require 'bundler/setup'
require 'bundler'
require 'gollum/app'
#require 'gollum/frontend/app'

require 'sinatra'
require 'omniauth'
require 'omniauth-slack'
require 'yaml'
require 'pp'

=begin
get '/' do
	redirect '/auth/slack'
end
=end

gollum_path = File.expand_path( File.dirname( __FILE__ ) ) # CHANGE THIS TO POINT TO YOUR OWN WIKI REPO

$secrets_file = gollum_path + "/secrets.yaml"
$secrets = YAML.load_file( $secrets_file )

$stderr.printf( "gollum_path = %s\n", gollum_path )
$stderr.printf( "Gem.ruby = %s\n", Gem.ruby() )
$stderr.printf( "Gem.path = %s\n", Gem.path() )
#$stderr.printf( "Gem.home = %s\n", Gem.home() )

wiki_options = {}
#wiki_options[ :css ] = true # Equivalent to --css
#wiki_options[ :js ] = true # Equivalent to --js
#wiki_options[ :template_dir ] = path # Equivalent to --template-dir
#wiki_options[ :page_file_dir ] = path # Equivalent to --page-file-dir
#wiki_options[ :gollum_path ] = gollum_path # Equivalent to ARGV
#wiki_options[ :default_markup ] = :asciidoc
#wiki_options[ :ref ] = ref ## Equivalent to --ref
#wiki_options[ :repo_is_bare ] = true # Equivalent to --bare
#wiki_options[ :allow_editing ] = false # # Equivalent to --no-edit
wiki_options[ :live_preview ] = true # Equivalent to --live-preview
#wiki_options[ :allow_uploads ] = true # Equivalent to --allow-uploads
#wiki_options[ :per_page_uploads ] = true # When :allow_uploads is set, store uploads under a directory named after the page, as when using --allow-uploads page
#wiki_options[ :mathjax ] = true # Equivalent to --mathjax
#wiki_options[ :mathjax_config ] = source # Equivalent to --mathjax-config
#wiki_options[ :user_icons ] = source # Equivalent to --user-icons
#wiki_options[ :show_all ] = true # Equivalent to --show-all
#wiki_options[ :collapse_tree ] = true # Equivalent to --collapse-tree
#wiki_options[ :h1_title ] = true # Equivalent to --h1-title

Precious::App.set( :default_markup, :asciidoc )
Precious::App.set( :wiki_options, wiki_options )
Precious::App.set( :gollum_path, gollum_path )

def position()
	caller_line = caller.first.split(":")[1]
	return __FILE__ + ":" + caller_line
end

$stderr.printf( "%s: settings.class = %s\n", position(), settings.class )
$stderr.printf( "%s: settings.pretty_inspect = %s\n", position(), settings.pretty_inspect )

$stderr.printf( "%s: $secrets.pretty_inspect = %s\n", position(), $secrets.pretty_inspect )

$stderr.printf( "%s: here ...\n", position() )

module Precious
	class AppX < Precious::App

=begin
=end
		def position()
			caller_line = caller.first.split(":")[1]
			return __FILE__ + ":" + caller_line
		end

		use Rack::Session::Cookie,
			:key => 'rack.session',
			:secret => $secrets["session_secret"]

		use OmniAuth::Builder do
			provider :slack, $secrets["slack"]["api_key"], $secrets["slack"]["api_secret"], scope: "users:read", team: $secrets["slack"]["team_id"]
		end

		configure do
			enable :logging
			#set :sessions, true
		end
=begin
		get '*' do
			$stderr.printf( "%s: request.class = %s\n", position(), request.class )
			$stderr.printf( "%s: request.pretty_inspect = %s\n", position(), request.pretty_inspect )
		end
=end


=begin
=end

		before do
			$stderr.printf( "%s: request.class = %s\n", position(), request.class )
			$stderr.printf( "%s: request.pretty_inspect = %s\n", position(), request.pretty_inspect )
			$stderr.printf( "%s: session.class = %s\n", position(), session.class )
			$stderr.printf( "%s: session.pretty_inspect = %s\n", position(), session.pretty_inspect )
			if ( !File.fnmatch( "/auth/*", request.path_info ) )
				if ( session[ "email" ] && session["name"] )
					session[ "gollum.author" ] = { :name => session["name"], :email => session[ "email" ] }
					$stderr.printf( "%s: gollum.author = %s\n", position(), session[ "gollum.author" ].pretty_inspect )
				else
					redirect '/auth/slack'
				end
			end
		end

=begin
		get '/login' do
			$stderr.printf( "%s: request.class = %s\n", position(), request.class )
			$stderr.printf( "%s: request.pretty_inspect = %s\n", position(), request.pretty_inspect )
			$stderr.printf( "%s: session.class = %s\n", position(), session.class )
			$stderr.printf( "%s: session.pretty_inspect = %s\n", position(), session.pretty_inspect )
			<<-HTML
				<ul>
					<li><a href='/auth/slack'>Sign in with Slack</a></li>
				</ul>
			HTML
		end
=end

		get '/logout' do
			session[ "email" ] = false
			session[ "name" ] = false
			content_type 'text/plain'
			"Goodbye ..."
		end


=begin
		get '/' do
			logger.info( sprintf( "%s: here ...\n", position() ) )
			$stderr.printf( "%s: request.class = %s\n", position(), request.class )
			$stderr.printf( "%s: request.pretty_inspect = %s\n", position(), request.pretty_inspect )
			$stderr.printf( "%s: session.class = %s\n", position(), session.class )
			$stderr.printf( "%s: session.pretty_inspect = %s\n", position(), session.pretty_inspect )
			logger.info( sprintf( "%s: request.class = %s\n", position(), request.class ) )
			logger.info( sprintf( "%s: request.pretty_inspect = %s\n", position(), request.pretty_inspect ) )
			logger.info( sprintf( "%s: session.class = %s\n", position(), session.class ) )
			logger.info( sprintf( "%s: session.pretty_inspect = %s\n", position(), session.pretty_inspect ) )
			<<-HTML
				<ul>
					<li><a href='/auth/slack'>Sign in with Slack</a></li>
				</ul>
			HTML
		end
=end

=begin
		before do
		end
=end

		get '/auth/:provider/callback' do
			logger.info( sprintf( "%s: here ...\n", position() ) )
			logger.info( sprintf( "%s: request.class = %s\n", position(), request.class ) )
			logger.info( sprintf( "%s: request.pretty_inspect = %s\n", position(), request.pretty_inspect ) )
			logger.info( sprintf( "%s: session.class = %s\n", position(), session.class ) )
			logger.info( sprintf( "%s: session.pretty_inspect = %s\n", position(), session.pretty_inspect ) )
			#content_type 'text/plain'
			#request.env['omniauth.auth'].to_hash.inspect rescue "No Data"
			session[:name] = request.env[ "omniauth.auth" ][ "info" ][ "name" ]
			session[:email] = request.env[ "omniauth.auth" ][ "info" ][ "email" ]
			redirect "/"
		end

		get '/auth/failure' do
			logger.info( sprintf( "%s: here ...\n", position() ) )
			logger.info( sprintf( "%s: request.class = %s\n", position(), request.class ) )
			logger.info( sprintf( "%s: request.pretty_inspect = %s\n", position(), request.pretty_inspect ) )
			logger.info( sprintf( "%s: session.class = %s\n", position(), session.class ) )
			logger.info( sprintf( "%s: session.pretty_inspect = %s\n", position(), session.pretty_inspect ) )
			content_type 'text/plain'
			#request.env['omniauth.auth'].to_hash.inspect rescue "No Data"
			"Nope"
		end
	end
end

$stderr.printf( "%s: here ...\n", position() )

$stderr.printf( "Running app ...\n" )

run Precious::AppX
